package top.rocket307.zuul.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.rocket307.zuul.dao.UserDao;
import top.rocket307.zuul.entity.UserLogin;
import top.rocket307.zuul.service.AuthService;

@Service
public class AuthServiceImpl implements AuthService {

    @Autowired
    UserDao userDao;

    @Override
    public UserLogin getUserLoginByName(String authenticationUser) {
        return userDao.getUserLoginByName(authenticationUser);
    }

    @Override
    public void login(Integer userId) {
        userDao.login(userId);
    }
}
