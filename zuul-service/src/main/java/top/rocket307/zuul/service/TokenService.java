package top.rocket307.zuul.service;

import top.rocket307.zuul.exception.IllegalTokenException;

import javax.servlet.http.HttpServletRequest;

public interface TokenService {
    String getToken(String userId);
    String verifyToken(HttpServletRequest request) throws IllegalTokenException;
}
