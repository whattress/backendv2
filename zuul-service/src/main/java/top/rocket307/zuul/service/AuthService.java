package top.rocket307.zuul.service;

import top.rocket307.zuul.entity.UserLogin;

public interface AuthService {
    UserLogin getUserLoginByName(String authenticationUser);
    void login(Integer userId);
}
