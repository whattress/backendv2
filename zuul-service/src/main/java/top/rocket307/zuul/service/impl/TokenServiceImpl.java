package top.rocket307.zuul.service.impl;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.springframework.stereotype.Service;
import top.rocket307.common.utils.Base64Util;
import top.rocket307.common.utils.TokenUtil;
import top.rocket307.zuul.exception.IllegalTokenException;
import top.rocket307.zuul.service.TokenService;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

@Service
public class TokenServiceImpl implements TokenService {

    @Override
    public String getToken(String userId) {
        String token;
        Date start = new Date();
        long currentTime = System.currentTimeMillis() + 60* 60 * 1000;//一小时有效时间
        Date end = new Date(currentTime);
        token = JWT.create().withAudience(userId)
                .sign(Algorithm.HMAC256("callMeDaddy"));
        return token;
    }

    @Override
    public String verifyToken(HttpServletRequest request) throws IllegalTokenException {
        String userId,token;
        try {
            token = TokenUtil.getToken(request);
            DecodedJWT jwt = JWT.decode(token);
            System.out.println(Base64Util.decode(jwt.getPayload()));
            userId = TokenUtil.getTokenUserId(token);
            Algorithm algorithm = Algorithm.HMAC256("callMeDaddy");
            JWTVerifier verifier = JWT.require(algorithm)
                    .withAudience(userId)
                    .build(); //Reusable verifier instance
            verifier.verify(token);
        } catch (JWTVerificationException | NullPointerException exception){
            exception.printStackTrace();
            throw new IllegalTokenException(404, "会话已失效，请重新登录");
        }
        return userId;
    }
}
