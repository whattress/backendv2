package top.rocket307.zuul.dao;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import top.rocket307.zuul.entity.UserLogin;

@Repository
@Mapper
public interface UserDao {
    UserLogin getUserLoginByName(String authenticationUser);
    void login(Integer userId);
}
