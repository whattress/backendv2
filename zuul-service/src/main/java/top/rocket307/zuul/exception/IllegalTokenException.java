package top.rocket307.zuul.exception;

public class IllegalTokenException extends Exception {
    private String message;
    private Integer status;

    public IllegalTokenException(Integer status, String message){
        super(message);
        this.message=message;
        this.status=status;
    }

}
