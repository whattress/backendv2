package top.rocket307.zuul.exception;

import org.springframework.dao.DataAccessException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import top.rocket307.common.dto.BaseResult;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(IllegalTokenException.class)
    @ResponseBody
    public IllegalTokenException TokenExceptionHandler(IllegalTokenException e){
        e.printStackTrace();
        return e;
    }

    @ExceptionHandler(DataAccessException.class)
    @ResponseBody
    public BaseResult<Object> sqlException(DataAccessException e) {
        e.printStackTrace();
        return new BaseResult<Object>(401,"服务器开小差");
    }

    /**
     * 拦截未知的运行时异常
     */
    @ExceptionHandler(RuntimeException.class)
    @ResponseBody
    public BaseResult<Object> notFoundException(RuntimeException e) {
        e.printStackTrace();
        return new BaseResult<Object>(401,"服务器开小差");
    }
}
