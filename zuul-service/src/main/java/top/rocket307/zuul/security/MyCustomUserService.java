package top.rocket307.zuul.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import top.rocket307.zuul.dto.ClassmateUserDetails;
import top.rocket307.zuul.entity.UserLogin;
import top.rocket307.zuul.service.AuthService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
public class MyCustomUserService implements UserDetailsService {

    @Autowired
    private AuthService authService;

    /**
     * 登陆验证时，通过username获取用户的所有权限信息
     * 并返回UserDetails放到spring的全局缓存SecurityContextHolder中，以供授权器使用
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        ClassmateUserDetails userDetails = new ClassmateUserDetails();
        userDetails.setUsername(username);
        System.out.println(username);
        UserLogin user=null;
        try {
            user = authService.getUserLoginByName(username);
            authService.login(user.getUserId());
//            user = new UserLogin(1,1,"email","user",
//                    "$2a$10$wMO86EylQ6SXwFt93G/4S.njzOQ1F527vXcdZnp6ZC7vt76YQBfWC","admin");
            System.out.println(user);
            //记录登录时间
        }catch (UsernameNotFoundException u){
            u.printStackTrace();
        }
        if (user!=null) {
            userDetails.setPassword(user.getAuthenticationCredential());
            userDetails.setUserId(user.getUserId());
            System.out.println(userDetails.getPassword());
            List<SimpleGrantedAuthority> roles = new ArrayList<>(Collections.singletonList(new SimpleGrantedAuthority("ROLE_USER")));
            if (user.getUserRole().equals("admin")) {
                roles.add(new SimpleGrantedAuthority("ROLE_ADMIN"));    //此处需加ROLE_
            }
            userDetails.setAuthorities(roles);
        }
        return userDetails;
    }
}
