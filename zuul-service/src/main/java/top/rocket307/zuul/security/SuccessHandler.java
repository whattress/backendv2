package top.rocket307.zuul.security;

import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import springfox.documentation.annotations.ApiIgnore;
import top.rocket307.zuul.dto.ClassmateUserDetails;
import top.rocket307.zuul.service.TokenService;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

@ApiIgnore
public class SuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {

    @Autowired
    TokenService tokenService;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        response.setContentType("application/json;charset=utf-8");
        Object principal = authentication.getPrincipal();
        String token = tokenService.getToken(((ClassmateUserDetails)principal).getUserId().toString());
        Cookie cookie=new Cookie("token", token);
        response.addCookie(cookie);
        HashMap<String, Object> map = new HashMap<String, Object>(){
            {
                put("code",200);
                put("msg","登录成功");
            }
        };
        PrintWriter out = response.getWriter();
        out.write(JSON.toJSONString(map));
        out.flush();
    }
}