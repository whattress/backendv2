package top.rocket307.common.utils;

import com.auth0.jwt.JWT;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

public class TokenUtil {

    public static String getTokenUserId(String token) {
        return JWT.decode(token).getAudience().get(0);
    }

    /**
     * 获取request
     *
     * @return
     */
    public static String getToken(HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();
        String token=null;
        if (cookies!=null){
            for (Cookie cookie : cookies){
                if (cookie.getName().equals("token")){
                    token=cookie.getValue();
                    break;
                }
            }
        }
        return token;
    }
}
