package top.rocket307.common.utils;

import org.springframework.util.DigestUtils;

public class MD5Util {

    public static String getMd5(String message, String salt){
        String base = message + "/" + salt;
        return DigestUtils.md5DigestAsHex(base.getBytes());
    }
}
