package top.rocket307.common.utils;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

public class Base64Util {
    public static String encode(String s){
        return Base64.getEncoder().encodeToString(s.getBytes(StandardCharsets.UTF_8));
    }
    public static String decode(String s){
        return new String(Base64.getDecoder().decode(s), StandardCharsets.UTF_8);
    }
}
