package top.rocket307.common.utils;

/**
 * 不同环境配置类
 */
public class ConfigUtil {
    public final static String LINUX_IMAGE_PATH="/usr/local/image/";
    public final static String WINDOWS_IMAGE_PATH="/E:/BackEnd/image/";
    public final static String WINDOWS_MAIL_PATH="http://localhost:8080/verify/";
    public final static String LINUX_MAIL_PATH="http://47.103.11.5:8080/classmate/verify/";
}
