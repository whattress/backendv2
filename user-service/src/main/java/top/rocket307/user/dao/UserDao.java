package top.rocket307.user.dao;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import top.rocket307.user.dto.FollowFans;
import top.rocket307.user.entity.UserLogin;
import top.rocket307.user.entity.UserProfile;

import java.util.List;

@Repository
@Mapper
public interface UserDao {
    UserLogin getUserLoginByName(String authenticationUser);
    void updateUserProfile(UserProfile userProfile);
    void saveUserLogin(UserLogin userLogin);
    void saveUserProfile(String username, String now);
    Integer getIdByName(String username);
    void verify(String userId);
    UserProfile getUserProfileById(Integer userId);
    String getProfilePathById(Integer userId);
    void saveFollow(Integer userId, Integer followId);
    void saveFans(Integer userId, Integer fansId);
    void removeFollow(Integer userId, Integer followId);
    void removeFans(Integer userId, Integer fansId);
    List<FollowFans> getFollow(Integer userId);
    List<FollowFans> getFans(Integer userId);
    void login(Integer userId);
    void uploadProfile(Integer userId, String relativePath);
}
