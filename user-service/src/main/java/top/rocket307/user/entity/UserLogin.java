package top.rocket307.user.entity;

import java.io.Serializable;


public class UserLogin implements Serializable, Cloneable {
    /** id */
    private Integer id ;
    /** 用户id */
    private Integer userId ;
    /** 验证方式 */
    private String authenticationType ;
    /** 验证人 */
    private String authenticationUser ;
    /** 凭证 */
    private String authenticationCredential ;
    /** 角色 */
    private String userRole ;

    /** id */
    public Integer getId(){
        return this.id;
    }
    /** id */
    public void setId(Integer id){
        this.id = id;
    }
    /** 用户id */
    public Integer getUserId(){
        return this.userId;
    }
    /** 用户id */
    public void setUserId(Integer userId){
        this.userId = userId;
    }
    /** 验证方式 */
    public String getAuthenticationType(){
        return this.authenticationType;
    }
    /** 验证方式 */
    public void setAuthenticationType(String authenticationType){
        this.authenticationType = authenticationType;
    }
    /** 验证人 */
    public String getAuthenticationUser(){
        return this.authenticationUser;
    }
    /** 验证人 */
    public void setAuthenticationUser(String authenticationUser){
        this.authenticationUser = authenticationUser;
    }
    /** 凭证 */
    public String getAuthenticationCredential(){
        return this.authenticationCredential;
    }
    /** 凭证 */
    public void setAuthenticationCredential(String authenticationCredential){
        this.authenticationCredential = authenticationCredential;
    }
    /** 角色 */
    public String getUserRole(){
        return this.userRole;
    }
    /** 角色 */
    public void setUserRole(String userRole){
        this.userRole = userRole;
    }

}