package top.rocket307.user.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel("用户资料")
public class UserProfile implements Serializable, Cloneable {
    /** 用户id */
    private Integer userId ;
    /** 用户学号 */
    @ApiModelProperty(value = "学号")
    private String userNumber ;
    /** 用户名 */
    @ApiModelProperty(value = "昵称")
    private String userName ;
    /** 用户学校 */
    @ApiModelProperty(value = "学校")
    private String userSchool ;
    /** 用户专业 */
    @ApiModelProperty(value = "专业")
    private String userMajor ;
    /** 用户城市 */
    @ApiModelProperty(value = "城市")
    private String userCity ;
    /** 用户签名 */
    @ApiModelProperty(value = "签名")
    private String userMotto ;
    /** 用户头像 */
    @ApiModelProperty(value = "头像（传空字符串，另有方法上传头像）")
   private String userProfile ;
    /** 用户番茄时长 */
    @ApiModelProperty(value = "番茄时长（无法更改，传空字符串）")
    private Integer userTomato ;
    /** 注册时间 */
    @ApiModelProperty(value = "空字符串")
    private String registrationTime ;
    /** 上次登录时间 */
    @ApiModelProperty(value = "空字符串")
    private String lastLoginTime ;

    /** 用户id */
    public Integer getUserId(){
        return this.userId;
    }
    /** 用户id */
    public void setUserId(Integer userId){
        this.userId = userId;
    }
    /** 用户学号 */
    public String getUserNumber(){
        return this.userNumber;
    }
    /** 用户学号 */
    public void setUserNumber(String userNumber){
        this.userNumber = userNumber;
    }
    /** 用户名 */
    public String getUserName(){
        return this.userName;
    }
    /** 用户名 */
    public void setUserName(String userName){
        this.userName = userName;
    }
    /** 用户学校 */
    public String getUserSchool(){
        return this.userSchool;
    }
    /** 用户学校 */
    public void setUserSchool(String userSchool){
        this.userSchool = userSchool;
    }
    /** 用户专业 */
    public String getUserMajor(){
        return this.userMajor;
    }
    /** 用户专业 */
    public void setUserMajor(String userMajor){
        this.userMajor = userMajor;
    }
    /** 用户城市 */
    public String getUserCity(){
        return this.userCity;
    }
    /** 用户城市 */
    public void setUserCity(String userCity){
        this.userCity = userCity;
    }
    /** 用户签名 */
    public String getUserMotto(){
        return this.userMotto;
    }
    /** 用户签名 */
    public void setUserMotto(String userMotto){
        this.userMotto = userMotto;
    }
    /** 用户头像 */
    public String getUserProfile(){
        return this.userProfile;
    }
    /** 用户头像 */
    public void setUserProfile(String userProfile){
        this.userProfile = userProfile;
    }
    /** 用户番茄时长 */
    public Integer getUserTomato(){
        return this.userTomato;
    }
    /** 用户番茄时长 */
    public void setUserTomato(Integer userTomato){
        this.userTomato = userTomato;
    }
    /** 注册时间 */
    public String getRegistrationTime(){
        return this.registrationTime;
    }
    /** 注册时间 */
    public void setRegistrationTime(String registrationTime){
        this.registrationTime = registrationTime;
    }
    /** 上次登录时间 */
    public String getLastLoginTime(){
        return this.lastLoginTime;
    }
    /** 上次登录时间 */
    public void setLastLoginTime(String lastLoginTime){
        this.lastLoginTime = lastLoginTime;
    }
}