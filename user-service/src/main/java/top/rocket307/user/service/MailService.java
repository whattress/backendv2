package top.rocket307.user.service;

public interface MailService {
    void sendSimpleMail(String subject, String text, String to);
}
