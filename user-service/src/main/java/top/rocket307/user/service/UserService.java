package top.rocket307.user.service;

import org.springframework.web.multipart.MultipartFile;
import top.rocket307.common.dto.BaseResult;
import top.rocket307.user.dto.FollowFans;
import top.rocket307.user.entity.UserLogin;
import top.rocket307.user.entity.UserProfile;

import java.util.List;

public interface UserService {
    UserLogin getUserLoginByName(String authenticationUser);
    BaseResult<Object> updateProfile(UserProfile userProfile);
    void saveUser(UserLogin user);
    void verify(String userEmail);
    UserProfile getUserProfileById(Integer userId);
    void saveFollow(Integer userId, Integer followId);
    void removeFollow(Integer userId, Integer followId);
    List<FollowFans> getFollow(Integer userId);
    List<FollowFans> getFans(Integer userId);
    String uploadProfile(Integer userId, MultipartFile file);
}
