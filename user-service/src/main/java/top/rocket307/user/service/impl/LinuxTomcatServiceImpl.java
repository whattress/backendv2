package top.rocket307.user.service.impl;

import org.springframework.context.annotation.Conditional;
import org.springframework.stereotype.Service;
import top.rocket307.common.condition.LinuxCondition;
import top.rocket307.common.utils.ConfigUtil;
import top.rocket307.user.service.TomcatService;

@Service
@Conditional(LinuxCondition.class)
public class LinuxTomcatServiceImpl implements TomcatService {
    @Override
    public String getImagePath() {
        return ConfigUtil.LINUX_IMAGE_PATH;
    }

    @Override
    public String getMAILPath() {
        return ConfigUtil.LINUX_MAIL_PATH;
    }
}