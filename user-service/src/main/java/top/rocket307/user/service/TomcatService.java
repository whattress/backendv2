package top.rocket307.user.service;

public interface TomcatService {
    String getImagePath();
    String getMAILPath();
}
