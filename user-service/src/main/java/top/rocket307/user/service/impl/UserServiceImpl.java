package top.rocket307.user.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import top.rocket307.common.dto.BaseResult;
import top.rocket307.common.utils.FileUtils;
import top.rocket307.user.dao.UserDao;
import top.rocket307.user.dto.FollowFans;
import top.rocket307.user.entity.UserLogin;
import top.rocket307.user.entity.UserProfile;
import top.rocket307.user.service.TomcatService;
import top.rocket307.user.service.UserService;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserDao userDao;
    @Autowired
    TomcatService tomcatService;

    @Override
    public UserLogin getUserLoginByName(String authenticationUser) {
        return userDao.getUserLoginByName(authenticationUser);
    }

    @Override
    public BaseResult<Object> updateProfile(UserProfile userProfile) {
        HashMap<String, Object> res = new HashMap<>();
        try{
            userDao.updateUserProfile(userProfile);
            return new BaseResult<>(200, "修改成功");
        }catch (Exception e){
            e.printStackTrace();
            return new BaseResult<>(404, "修改失败");
        }
    }

    @Override
    public void saveUser(UserLogin user) {
        //先插入用户资料，并获得user_id
        String username=user.getAuthenticationUser();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date= new Date();
        String now = sdf.format(date);
        userDao.saveUserProfile(username, now);
        user.setUserId(userDao.getIdByName(username));
        //加上user_id后，插入用户认证表
        userDao.saveUserLogin(user);
    }

    @Override
    public void verify(String userEmail){
        userDao.verify(userEmail);
    }

    @Override
    public UserProfile getUserProfileById(Integer userId) {
        return userDao.getUserProfileById(userId);
    }

    @Override
    public void saveFollow(Integer userId, Integer followId) {
        //分别把信息存入关注表和粉丝表，分成两张表是为了便于实现分布式分表以后查找
        userDao.saveFollow(userId, followId);
        userDao.saveFans(followId, userId);
    }

    @Override
    public void removeFollow(Integer userId, Integer followId) {
        userDao.removeFollow(userId, followId);
        userDao.removeFans(followId, userId);
    }

    @Override
    public List<FollowFans> getFollow(Integer userId) {
        return userDao.getFollow(userId);
    }

    @Override
    public List<FollowFans> getFans(Integer userId) {
        return userDao.getFans(userId);
    }

    @Override
    public String uploadProfile(Integer userId, MultipartFile file) {
        // 要上传的目标文件存放的绝对路径
        final String localPath=tomcatService.getImagePath();
        System.out.println(localPath);
        //上传后保存的文件名(需要防止图片重名导致的文件覆盖)
        //获取文件名
        String fileName = file.getOriginalFilename();
        //获取文件后缀名
        String suffixName = fileName.substring(fileName.lastIndexOf("."));
        //重新生成文件名
        fileName = UUID.randomUUID()+suffixName;
        if (FileUtils.upload(file, localPath, fileName)) {
            //文件存放的相对路径(一般存放在数据库用于img标签的src)
            try {
                String lastFileName = userDao.getProfilePathById(userId);
                FileUtils.deleteFile(localPath,lastFileName);
                userDao.uploadProfile(userId, fileName);
            } catch (Exception e){
                e.printStackTrace();
                FileUtils.deleteFile(localPath,fileName);
            }
            return fileName;
        }
        FileUtils.deleteFile(localPath,fileName);
        return "fail";
    }
}
