package top.rocket307.user.service.impl;

import org.springframework.context.annotation.Conditional;
import org.springframework.stereotype.Service;
import top.rocket307.common.condition.WindowsCondition;
import top.rocket307.common.utils.ConfigUtil;
import top.rocket307.user.service.TomcatService;

@Service
@Conditional(WindowsCondition.class)
public class WindowsTomcatServiceImpl implements TomcatService {

    @Override
    public String getImagePath() {
        return ConfigUtil.WINDOWS_IMAGE_PATH;
    }

    @Override
    public String getMAILPath() {
        return ConfigUtil.WINDOWS_MAIL_PATH;
    }

}
