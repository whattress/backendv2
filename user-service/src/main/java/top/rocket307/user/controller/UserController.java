package top.rocket307.user.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import top.rocket307.common.dto.BaseResult;
import top.rocket307.common.utils.Base64Util;
import top.rocket307.user.dto.FollowFans;
import top.rocket307.user.entity.UserLogin;
import top.rocket307.user.entity.UserProfile;
import top.rocket307.user.service.MailService;
import top.rocket307.user.service.TomcatService;
import top.rocket307.user.service.UserService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Api(value = "用户路由控制器",tags = "用户路由控制器")
@RestController
public class UserController {

    @Autowired
    MailService mailService;
    @Autowired
    UserService userService;
//    @Autowired
//    TokenService tokenService;
    @Autowired
    TomcatService tomcatService;

//    @ApiOperation("获取用户资料")
//    @GetMapping("getProfile")
//    public BaseResult<Object> getProfile(HttpServletRequest request) throws IllegalTokenException {
//        UserProfile userProfile = userService.getUserProfileById(Integer.valueOf(tokenService.verifyToken(request)));
//        return new BaseResult<>(200, userProfile);
//    }

    @ApiOperation(value = "修改用户资料")
    @PutMapping("updateProfile")
    public BaseResult<Object> updateProfile(@RequestBody @ApiParam(name="用户资料对象",value="传入json格式",required=true)
                                                        UserProfile userProfile){
        return  userService.updateProfile(userProfile);
    }

    @ApiOperation(value = "注册")
    @PostMapping("register")
    public BaseResult<Object> register(@RequestBody @ApiParam(name="用户认证对象",value="传入json格式",required=true)
                                               UserLogin user){
        user.setUserRole("");
        if (user.getAuthenticationType().equals("email")){
            try{
                userService.saveUser(user);
                String content = "你好，账号激活请点击：" + tomcatService.getMAILPath() + Base64Util.encode(user.getAuthenticationUser());
                mailService.sendSimpleMail("class&mate用户激活", content, user.getAuthenticationUser());
                System.out.println("****************"+user.getAuthenticationUser());
                return new BaseResult<>(200, "注册成功");
            }catch (Exception e){
                return new BaseResult<>(401, "注册失败，请稍后再试");
            }
        }
        return new BaseResult<>(401, "注册失败，请稍后再试");
    }

    @ApiOperation(value = "认证（后台通过发送邮件处理）")
    @PutMapping("verify/{userEmail}")
    public BaseResult<Object> verify(@PathVariable String userEmail){
        userService.verify(Base64Util.decode(userEmail));
        return new BaseResult<>(200, "认证成功");
    }

//    @ApiOperation(value = "关注")
//    @PostMapping("saveFollow")
//    public BaseResult<Object> saveFollow(HttpServletRequest request, String followId) throws IllegalTokenException {
//        userService.saveFollow(Integer.valueOf(tokenService.verifyToken(request)), Integer.valueOf(followId));
//        return new BaseResult<>(200, "关注成功");
//    }

//    @ApiOperation(value = "取消关注")
//    @DeleteMapping("removeFollow")
//    public BaseResult<Object> removeFollow(HttpServletRequest request, String followId) throws IllegalTokenException {
//        userService.removeFollow(Integer.valueOf(tokenService.verifyToken(request)), Integer.valueOf(followId));
//        return new BaseResult<>(200, "取消关注成功");
//    }
//
//    @ApiOperation(value = "我的关注")
//    @GetMapping("getFollow")
//    public BaseResult<Object> getFollow(HttpServletRequest request) throws IllegalTokenException {
//        List<FollowFans> follow = userService.getFollow(Integer.valueOf(tokenService.verifyToken(request)));
//        return new BaseResult<>(200, follow);
//    }
//
//    @ApiOperation(value = "我的粉丝")
//    @GetMapping("getFans")
//    public BaseResult<Object> getFans(HttpServletRequest request) throws IllegalTokenException {
//        List<FollowFans> fans = userService.getFans(Integer.valueOf(tokenService.verifyToken(request)));
//        return new BaseResult<>(200, fans);
//    }
//
//    @ApiOperation("上传头像")
//    @PutMapping("uploadProfile")
//    public BaseResult<Object> uploadProfile(@RequestParam(value = "file") MultipartFile file, HttpServletRequest request)
//            throws IllegalTokenException {
//        String fileType = file.getContentType();
//        System.out.println("*******************:"+fileType);
//        if (fileType.equals("image/jpg") || fileType.equals("image/png") || fileType.equals("image/jpeg")) {
//            String out = userService.uploadProfile(Integer.valueOf(tokenService.verifyToken(request)), file);
//            if(out.equals("fail")){
//                System.out.println("fail**********"+out);
//                return new BaseResult<>(404, "上传失败");
//            }
//            return new BaseResult<>(200, out);
//        }
//        return new BaseResult<>(403, "上传格式错误");
//    }


}
