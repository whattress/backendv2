package top.rocket307.classroom.dto;

import java.util.Date;

public class RoomWithLesson {

    private Integer relationId ;
    private Date date ;
    private Date lessonTime ;
    private Integer classroomId ;
    private Integer lessonId ;
    private Integer state ;

    private String occupationReason ;
    private String classrommName ;
    private Integer classroomType ;
    private String location ;
    private String lessonName ;


    public Integer getRelationId() {
        return relationId;
    }

    public void setRelationId(Integer relationId) {
        this.relationId = relationId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getLessonTime() {
        return lessonTime;
    }

    public void setLessonTime(Date lessonTime) {
        this.lessonTime = lessonTime;
    }

    public Integer getClassroomId() {
        return classroomId;
    }

    public void setClassroomId(Integer classroomId) {
        this.classroomId = classroomId;
    }

    public Integer getLessonId() {
        return lessonId;
    }

    public void setLessonId(Integer lessonId) {
        this.lessonId = lessonId;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getOccupationReason() {
        return occupationReason;
    }

    public void setOccupationReason(String occupationReason) {
        this.occupationReason = occupationReason;
    }

    public String getClassrommName() {
        return classrommName;
    }

    public void setClassrommName(String classrommName) {
        this.classrommName = classrommName;
    }

    public Integer getClassroomType() {
        return classroomType;
    }

    public void setClassroomType(Integer classroomType) {
        this.classroomType = classroomType;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLessonName() {
        return lessonName;
    }

    public void setLessonName(String lessonName) {
        this.lessonName = lessonName;
    }

}
