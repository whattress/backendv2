package top.rocket307.classroom.dto;

public class RoomOrdered {
    private String lessonTime ;
    private String occupationReason ;
    private Integer userId;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getLessonTime() {
        return lessonTime;
    }

    public void setLessonTime(String lessonTime) {
        this.lessonTime = lessonTime;
    }

    public String getOccupationReason() {
        return occupationReason;
    }

    public void setOccupationReason(String occupationReason) {
        this.occupationReason = occupationReason;
    }

    public String getClassrommName() {
        return classrommName;
    }

    public void setClassrommName(String classrommName) {
        this.classrommName = classrommName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    private String classrommName ;
    private String location ;
}
