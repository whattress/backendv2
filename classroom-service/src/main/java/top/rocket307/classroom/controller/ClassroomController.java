package top.rocket307.classroom.controller;


import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import top.rocket307.classroom.entity.StudyroomOrder;
import top.rocket307.classroom.entity.UserOccupation;
import top.rocket307.classroom.service.RoomLessonRelationService;
import top.rocket307.common.dto.BaseResult;

@Api(value = "教室路由控制器",tags = "教室路由控制器")
@RestController
//@RequestMapping("")
public class ClassroomController {

    @Autowired
    RoomLessonRelationService roomLessonRelationService;

    @GetMapping("hello")
    public String hello(){
        return "hello";
    }

//用户端
    //    查看教室
    @ApiOperation(value = "查看全部教室及相关情况")
    @GetMapping("listRoomWithLesson")
    public String listRoomWithLesson(){
        return JSONObject.toJSON(roomLessonRelationService.listClassroom()).toString();
    }


    //    占教室
    @ApiOperation(value = "占用教室")
    @PostMapping("updateRoomOccupated")
    public BaseResult<Object> updateRoomOccupated(@RequestBody @ApiParam(name="占教室对象",value="传入json格式",required=true)
                                                              UserOccupation userOccupation){
        if (roomLessonRelationService.updateOccupation(userOccupation)>0)
        {
            return new BaseResult<>(200, "成功");

        }
        else{
            return new BaseResult<>(404, "占教室已达上限，上限为2个");
        }
    }

    //    取消占教室
    @ApiOperation(value = "取消占用教室")
    @PostMapping("removeRoomOccupated")
    public BaseResult<Object> removeRoomOccupated(@RequestBody @ApiParam(name="取消占教室对象",value="传入json格式",required=true)
                                                              UserOccupation userOccupation){
        roomLessonRelationService.removeOccupation(userOccupation);
        return new BaseResult<>(200, "成功");
    }

//    //    查找已占用的教室
//    @ApiOperation(value = "查看已占用教室")
//    @PostMapping("listRoomtOccupation")
//    public String listRoomOccupation(@RequestBody @ApiParam(name="用户资料对象",value="传入json格式",required=true)
//                                                 UserLogin userLogin){
//        return JSONObject.toJSONString(roomLessonRelationService.listOccupation(userLogin.getUserId().toString()));
//    }

    //    预约自习教室
    @ApiOperation(value = "预约自习")
    @PostMapping("updateStudyroomOrder")
    public BaseResult<Object> updateStudyroomOrder(@RequestBody @ApiParam(name="预约自习对象",value="传入json格式",
            required=true)StudyroomOrder studyroomOrder){
        if (roomLessonRelationService.updateOrder(studyroomOrder)>0)
        {
            return new BaseResult<>(200, "成功");
        }
        else {
            return new BaseResult<>(404, "失败");
        }
    }

    //    取消预约自习室
    @ApiOperation(value = "取消预约自习")
    @PostMapping("removeStudyroomOrder")
    public BaseResult<Object> removeStudyroomOrder(@RequestBody @ApiParam(name="取消预约自习对象",value="传入json格式",
            required=true) StudyroomOrder studyroomOrder){
        roomLessonRelationService.removeOrder(studyroomOrder);
        return new BaseResult<>(200, "成功");
    }

//    //    查看已预约自习室
//    @ApiOperation(value = "查看已预约教室")
//    @PostMapping("listStudyroomOrder")
//    public String listStudyroomOrder(@RequestBody @ApiParam(name="用户资料对象",value="传入json格式",required=true)
//                                                 UserLogin userLogin){
//        return JSONObject.toJSONString(roomLessonRelationService.listOrder(userLogin.getUserId().toString()));
//    }



//管理员端

    //     管理员查看多媒体教室借用申请
    @ApiOperation(value = "管理员查看多媒体教室申请")
    @GetMapping("listRoomApplication")
    public String listRoomApplication(){
        return JSONObject.toJSONString(roomLessonRelationService.listApplication());
    }

    //     管理员通过多媒体教室借用申请
    @ApiOperation(value = "管理员通过多媒体教室申请")
    @GetMapping("updateRoomApplicationAccept")
    public BaseResult<Object> updateRoomApplicationAccept(@RequestBody @ApiParam(name="管理多媒体教室申请对象",
            value="传入json格式",required=true)UserOccupation userOccupation){
        roomLessonRelationService.updateApplicationAccept(userOccupation);
        return new BaseResult<>(200, "成功");
    }

    //     管理员拒绝多媒体教室借用申请
    @ApiOperation(value = "管理员拒绝多媒体教室申请")
    @GetMapping("updateRoomApplicationRefuse")
    public BaseResult<Object> updateRoomApplicationRefuse(@RequestBody @ApiParam(name="管理多媒体教室申请对象",
            value="传入json格式",required=true)UserOccupation userOccupation){
        roomLessonRelationService.updateApplicationRefuse(userOccupation);
        return new BaseResult<>(200, "成功");
    }

    //     管理员上传课表

}
