package top.rocket307.classroom.service;

import top.rocket307.classroom.dto.RoomOccupated;
import top.rocket307.classroom.dto.RoomOrdered;
import top.rocket307.classroom.dto.RoomWithLesson;
import top.rocket307.classroom.entity.StudyroomOrder;
import top.rocket307.classroom.entity.UserOccupation;

import java.util.List;

public interface RoomLessonRelationService {

    //    客户端
    List<RoomWithLesson> listClassroom();
    int updateOccupation(UserOccupation userOccupation);
    void removeOccupation(UserOccupation userOccupation);
    List<RoomOccupated> listOccupation(String userId);
    int updateOrder(StudyroomOrder studyroomOrder);
    void removeOrder(StudyroomOrder studyroomOrder);
    List<RoomOrdered> listOrder(String userId);

    //    管理员端
    List<RoomOccupated> listApplication();
    void updateApplicationAccept(UserOccupation userOccupation);
    void updateApplicationRefuse(UserOccupation userOccupation);

}
