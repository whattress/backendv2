package top.rocket307.classroom.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.rocket307.classroom.dao.ClassroomLessonRelationDao;
import top.rocket307.classroom.dto.RoomOccupated;
import top.rocket307.classroom.dto.RoomOrdered;
import top.rocket307.classroom.dto.RoomWithLesson;
import top.rocket307.classroom.entity.StudyroomOrder;
import top.rocket307.classroom.entity.UserOccupation;
import top.rocket307.classroom.service.RoomLessonRelationService;

import java.util.List;

@Service
public class RoomLessonRelationServiceImpl implements RoomLessonRelationService {

    @Autowired
    ClassroomLessonRelationDao classroomLessonRelationDao;

//客户端
    //    将教室（有课、无课、以及被占）展示数据传递给界面层
    @Override
    public List<RoomWithLesson> listClassroom(){
        return classroomLessonRelationDao.listRoomLessonRelation();
    }

    //    占教室
    @Override
    public int updateOccupation(UserOccupation userOccupation){
        if (classroomLessonRelationDao.countClassroomOccupation(userOccupation) >= 2)
        {
            return 0;
        }
        else {
            if (userOccupation.getClassroomType() == 1)
            {
                classroomLessonRelationDao.updateClassroomOccupationSecond(userOccupation);
            }
            else {
                classroomLessonRelationDao.updateClassroomOccupationFirst(userOccupation);
                classroomLessonRelationDao.updateClassroomOccupationSecond(userOccupation);
            }
        }
        return 1;
    }

    //     取消占教室
    public void removeOccupation(UserOccupation userOccupation) {
        classroomLessonRelationDao.removeClassroomOccupationFirst(userOccupation);
        classroomLessonRelationDao.removeClassroomOccupationSecond(userOccupation);
    }

    //    展示已占用的教室
    @Override
    public List<RoomOccupated> listOccupation(String userId) {
        return classroomLessonRelationDao.listClassroomOccupation(userId);
    }

    //    预约自习教室
    @Override
    public int updateOrder(StudyroomOrder studyroomOrder) {
        if (classroomLessonRelationDao.countClassroomOrder(studyroomOrder) != 0)
        {
            return 0;
        }
        else
        {
            classroomLessonRelationDao.updateClassroomOrder(studyroomOrder);
        }
        return 1;
    }

    //    取消预约自习教室
    @Override
    public void removeOrder(StudyroomOrder studyroomOrder) {
        classroomLessonRelationDao.removeClassroomOrder(studyroomOrder);
    }

    //    展示已预约自习的教室
    @Override
    public List<RoomOrdered> listOrder(String userId) {
        return classroomLessonRelationDao.listClassroomOrder(userId);
    }

//管理员端
    //    查看多媒体教室申请
    public List<RoomOccupated> listApplication() {
        return classroomLessonRelationDao.listClassroomApplication();
    }

    //    通过多媒体教室申请
    public void updateApplicationAccept(UserOccupation userOccupation) {
        classroomLessonRelationDao.updateClassroomApplicationAccept(userOccupation);
    }
    //    拒绝多媒体教室申请
    public void updateApplicationRefuse(UserOccupation userOccupation) {
        classroomLessonRelationDao.updateClassroomApplicationRefuse(userOccupation);
    }

}
