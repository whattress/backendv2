package top.rocket307.classroom.entity;

import java.io.Serializable;


public class UserOccupation implements Serializable, Cloneable {
    /** 用户序号 */
    private Integer userId ;
    /** 关系序号 */
    private Integer relationId ;
    /** 教室类别 */
    private Integer classroomType ;
    /** 占用理由 */
    private String occupationReason ;

    /** 用户序号 */
    public Integer getUserId(){
        return this.userId;
    }
    /** 用户序号 */
    public void setUserId(Integer userId){
        this.userId = userId;
    }
    /** 关系序号 */
    public Integer getRelationId(){
        return this.relationId;
    }
    /** 关系序号 */
    public void setRelationId(Integer relationId){
        this.relationId = relationId;
    }
    /** 教室类别 */
    public Integer getClassroomType(){
        return this.classroomType;
    }
    /** 教室类别 */
    public void setClassroomType(Integer classroomType){
        this.classroomType = classroomType;
    }
    /** 占用理由 */
    public String getOccupationReason(){
        return this.occupationReason;
    }
    /** 占用理由 */
    public void setOccupationReason(String occupationReason){
        this.occupationReason = occupationReason;
    }
}