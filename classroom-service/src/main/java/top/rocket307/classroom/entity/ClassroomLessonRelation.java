package top.rocket307.classroom.entity;

import java.io.Serializable;


public class ClassroomLessonRelation implements Serializable, Cloneable {
    /** 关系序号 */
    private Integer relationId ;
    /** 日期 */
    private String date ;
    /** 课程时间 */
    private String lessonTime ;
    /** 教室序号 */
    private Integer classroomId ;
    /** 课程序号 */
    private Integer lessonId ;
    /** 教室状态 */
    private Integer state ;

    /** 关系序号 */
    public Integer getRelationId(){
        return this.relationId;
    }
    /** 关系序号 */
    public void setRelationId(Integer relationId){
        this.relationId = relationId;
    }
    /** 日期 */
    public String getDate(){
        return this.date;
    }
    /** 日期 */
    public void setDate(String date){
        this.date = date;
    }
    /** 课程时间 */
    public String getLessonTime(){
        return this.lessonTime;
    }
    /** 课程时间 */
    public void setLessonTime(String lessonTime){
        this.lessonTime = lessonTime;
    }
    /** 教室序号 */
    public Integer getClassroomId(){
        return this.classroomId;
    }
    /** 教室序号 */
    public void setClassroomId(Integer classroomId){
        this.classroomId = classroomId;
    }
    /** 课程序号 */
    public Integer getLessonId(){
        return this.lessonId;
    }
    /** 课程序号 */
    public void setLessonId(Integer lessonId){
        this.lessonId = lessonId;
    }
    /** 教室状态 */
    public Integer getState(){
        return this.state;
    }
    /** 教室状态 */
    public void setState(Integer state){
        this.state = state;
    }
}