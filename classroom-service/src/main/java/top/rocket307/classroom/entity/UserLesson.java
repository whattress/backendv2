package top.rocket307.classroom.entity;

import java.io.Serializable;

public class UserLesson implements Serializable, Cloneable {
    /** 课程id */
    private Integer lessionId ;
    /** 用户id */
    private Integer userId ;
    /** 课程名称 */
    private String lessonName ;
    /** 课程时间 */
    private Integer lessonTime ;
    /** 开始周数 */
    private Integer beginWeek ;
    /** 结束周数 */
    private Integer endWeek ;

    /** 课程id */
    public Integer getLessionId(){
        return this.lessionId;
    }
    /** 课程id */
    public void setLessionId(Integer lessionId){
        this.lessionId = lessionId;
    }
    /** 用户id */
    public Integer getUserId(){
        return this.userId;
    }
    /** 用户id */
    public void setUserId(Integer userId){
        this.userId = userId;
    }
    /** 课程名称 */
    public String getLessonName(){
        return this.lessonName;
    }
    /** 课程名称 */
    public void setLessonName(String lessonName){
        this.lessonName = lessonName;
    }
    /** 课程时间 */
    public Integer getLessonTime(){
        return this.lessonTime;
    }
    /** 课程时间 */
    public void setLessonTime(Integer lessonTime){
        this.lessonTime = lessonTime;
    }
    /** 开始周数 */
    public Integer getBeginWeek(){
        return this.beginWeek;
    }
    /** 开始周数 */
    public void setBeginWeek(Integer beginWeek){
        this.beginWeek = beginWeek;
    }
    /** 结束周数 */
    public Integer getEndWeek(){
        return this.endWeek;
    }
    /** 结束周数 */
    public void setEndWeek(Integer endWeek){
        this.endWeek = endWeek;
    }
}