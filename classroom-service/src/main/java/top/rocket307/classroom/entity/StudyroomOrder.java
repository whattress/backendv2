package top.rocket307.classroom.entity;

import java.io.Serializable;

public class StudyroomOrder implements Serializable, Cloneable {
    /** 用户序号 */
    private Integer userId ;
    /** 关系序号 */
    private Integer relationId ;
    /** 课程时间 */
    private String lessonTime ;

    /** 用户序号 */
    public Integer getUserId(){
        return this.userId;
    }
    /** 用户序号 */
    public void setUserId(Integer userId){
        this.userId = userId;
    }
    /** 关系序号 */
    public Integer getRelationId(){
        return this.relationId;
    }
    /** 关系序号 */
    public void setRelationId(Integer relationId){
        this.relationId = relationId;
    }
    /** 课程时间 */
    public String getLessonTime(){
        return this.lessonTime;
    }
    /** 课程时间 */
    public void setLessonTime(String lessonTime){
        this.lessonTime = lessonTime;
    }
}