package top.rocket307.classroom.dao;


import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import top.rocket307.classroom.dto.RoomOccupated;
import top.rocket307.classroom.dto.RoomOrdered;
import top.rocket307.classroom.dto.RoomWithLesson;
import top.rocket307.classroom.entity.StudyroomOrder;
import top.rocket307.classroom.entity.UserOccupation;

import java.util.List;

@Repository
@Mapper
public interface ClassroomLessonRelationDao {

    //    客户端
    List<RoomWithLesson> listRoomLessonRelation();

    int countClassroomOccupation(UserOccupation userOccupation);
    void updateClassroomOccupationFirst(UserOccupation userOccupation);
    void updateClassroomOccupationSecond(UserOccupation userOccupation);
    void removeClassroomOccupationFirst(UserOccupation userOccupation);
    void removeClassroomOccupationSecond(UserOccupation userOccupation);
    List<RoomOccupated> listClassroomOccupation(String userId);

    int countClassroomOrder(StudyroomOrder studyroomOrder);
    void updateClassroomOrder(StudyroomOrder studyroomOrder);
    void removeClassroomOrder(StudyroomOrder studyroomOrder);
    List<RoomOrdered> listClassroomOrder(String userId);

    //    管理员端
    List<RoomOccupated> listClassroomApplication();
    void updateClassroomApplicationAccept(UserOccupation userOccupation);
    void updateClassroomApplicationRefuse(UserOccupation userOccupation);

}
